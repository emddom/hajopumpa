#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

#define mainHELLO_TASK_PRIORITY     ( tskIDLE_PRIORITY + 2 )
#define mainHELLO_DELAY         ( ( portTickType ) 100 / portTICK_RATE_MS )
#define mainWORLD_TASK_PRIORITY     ( tskIDLE_PRIORITY + 1 )
#define mainWORLD_DELAY         ( ( portTickType ) 500 / portTICK_RATE_MS )


void vHelloTask( void *pvParameters )
{
	portTickType xLastExecutionTime;
	/// Initialise xLastExecutionTime so the first call to vTaskDelayUntil() works correctly
    xLastExecutionTime = xTaskGetTickCount();
	vTaskDelayUntil( &xLastExecutionTime, mainHELLO_DELAY );
   printf("Hello ");
   
 
        
       
  vTaskDelete( NULL );
  
}

void vWorldTask( void *pvParameters )
{
    portTickType xLastExecutionTime;
	/// Initialise xLastExecutionTime so the first call to vTaskDelayUntil() works correctly
    xLastExecutionTime = xTaskGetTickCount();
	vTaskDelayUntil( &xLastExecutionTime, mainWORLD_DELAY );
    printf("World!");
        /// Delay mainCHECK_DELAY milliseconds.
  
       
    vTaskDelete( NULL );
}

void app_main(void)
{
	 
    /// Start the tasks defined within the file
    xTaskCreate( vHelloTask, "Hello", 10248, NULL, 5, NULL );
	//vTaskDelay(1000 / portTICK_RATE_MS);
	xTaskCreate( vWorldTask, "World", 10248, NULL, 5, NULL );
	vTaskDelay(1000 / portTICK_RATE_MS);
    printf("\nRestarting in 10 seconds.\n");
	vTaskDelay(1000 / portTICK_RATE_MS);
	for (int i = 9; i >= 0; i--) {
        printf("Restarting in %d seconds...\n", i);
        vTaskDelay(1000 / portTICK_RATE_MS);
    }
    printf("Restarting now.\n");
    fflush(stdout);
    esp_restart();
}
