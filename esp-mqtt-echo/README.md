#This is a MQTT Echo Server for ESP-32

To connect to a Broker, please specify the IP/hostname in idf.py menuconfig
I used a local mosquitto client for this.

Flash the program to device:
	idf monitor will show you what the ESP-32 reads.
	Sending data to the server will make ESP repeat the same
	string with 'ECHO:' written infront of it.
	
If using Mosquitto:
set the Broker IP in menuconfig to your PC's IPv4. (I used port 1883)
first subscribe to topic: topic/qos0
then publish to this topic
ESP will echo everything you publish.

When ESP connects to a topic, it will publish a notification.