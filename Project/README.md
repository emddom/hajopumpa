#Project:
The idea is to add more control over various components in a regular cruise boat. 

#Why:
The idea comes from a personal experience. Every year in summer, when the weather is lovely, I go on boat rides. 
However, when the weather is not so good, specially in August when we have storms in Estonia, the boat tends to take in quite a bit of water. 
There is a pump in the bilge, but it requires manual activation. 
Automatic systems for bilge pumps are quite common, but mostly quite flawed – they either start with every wave, even when the water level is low – or they are started when the water level gets high and then also deactivated before the water is pumped out. 
Having a microcontroller control the automatic process is the cornerstone of this project.

From the existing tasks I have reused some of the mqtt code.

#Components:
RPi – The RPi is situated inside the cabin. It can be used to control the audio system. It requires an USB soundcard to send the signal to the amp and stereo. The RPi is also running a local mosquitto broker (can be installed very easily from the terminal). All the information is relayed to proper topics thanks to NodeRED. NodeRED Dashboard can also be used to manually activate the bilgepump, if user finds it necessary.
The RPi is currently used in headless state, but there is an idea to add 2 monitors to it – one for the captain and one to the cabin (to listen to Spotify or watch a movie). There is an onboard router with 4G SIM that gives us both local WIFI and also connection to the Internet.

ESP32 – The ESP is the heart of the project. All the external sensors and actuators are run by ESP. The water level sensor is connected to it and if the water level should rise over the set limit (the limit depends on how and where the sensor is installed, usually on the boat floor, slightly angled. If the water level stays above the limit for 5 seconds (can be changed if the users finds it necessary, but with the waves in our rivers and sea, 5 seconds is okay) the pump is activated for a minimum of 30 seconds (after the water level drops under the limit, additional 30 seconds is necessary to make sure that the bilge is  pumped empty). 
Additionally, the cabin hatch is manually opened if the temperature gets too high (The hatch controls are specified later in the document, the system includes the raindrop sensor, temperature sensor and the hatch motor).

Water level sensor – A simple, robust sensor that connects the internal pins when the water levels rises (there is a little moving ball inside). The pictures are included inside the project folder.

Pump – There are many pumps for sale, my choice: https://www.autokaubad24.ee/et/tooted/pilsipumbad-kutusepumbad-vooliku-labiviigud/pilsipump-12v?fbclid=IwAR22CC7IK1LmG9JpbgwlRv7iSGtvQ5X1x-gvI38McbnM6xzWez4Kbf7xEPA
It is connected to the ESP by a relay, current is supplied by the auxiliary battery onboard the boat.

#Hatch Controls:

If the temperature gets above 30 degrees and it is not raining, the hatch must open. If the temperature falls under 30 degrees or it starts to rain, the hatch must close.
For rain, we use a simple raindrop sensor (note that while the boat is moving the automatic hatch system is offline – the automatic hatch is only needed when the boat is docked). For the temperature we use a bimetal temperature switch. Both normally closed or normally open types can be used. The current version of the code uses normally closed type.
One example is 	Cantherm CS703025Y or CS703025Z. 

The usage of the hatch motor is simple – it has 3 pins, 1 is ground. The other 2 move the motor, if 1 is activated then it opens the hatch and when the other one is activated then it closes the hatch (these pins are connected to the esp by a relay). The motor has internal shielding to stop when the hatch is fully closed/opened.


#Communication:
Most of the communication is done via mqtt.

The bilge – The state of the water level and pump is displayed on the NodeRED dashboard and is read from the esp via mqtt (local broker) on topics: /topic/esppump and /topic/espwater
	To activate the pump from long distance manually, the topic is: /topic/dashboard/pump
The current bilge state can also be read over the Internet. The RPi is connected to a cloud broker(broker.mqttdashboard.com) and published the state to: /topic/dashboard/pump
The hatch – Similar to the pump, the topic used here is: /topic/espwater
To see the state over the Internet, a message is published to the cloud server: /topic/boat/cloud/hatch/inquiry

#NodeRED – Currentyl there are 3 simple flows:
The hatch system – If there is a message to open/close the hatch then the message is published to the cloudmqtt server and also to the dashboard
The Bilge – Shows the current water level/pump status on the dashboard and also published to the cloudmqtt server
Pump switch – supplies a possibility to activate the bilgepump from the dashboard.

Demo of the water level sensor: https://youtu.be/LOTSa43xZkA
If the sensor rises then the ball drops to the other end, connecting the circuit. On the screen we can see that esp has detected a water level increase and after 5 secondsd activates the relay that connected the pump's electric circuit, activating the pump and publishing the active message to the mqtt topic.