#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_spi_flash.h"
#include "esp_adc_cal.h"
#include "protocol_examples_common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "driver/gpio.h"

///input and output ports - note that all outputs are connected to relays to ensure 12V to the pump and hatch motor

#define GPIO_PUMP    18
#define GPIO_HATCH1    0
#define GPIO_HATCH2    16
#define GPIO_TEMP    4
#define GPIO_HUMIDITY    5
#define GPIO_WATER     21

static const char *TAG = "MQTT_EXAMPLE";
int temprature_sens_read();
int hall_sens_read();
int msg_id = 0;
int pumpOpen = 0; // water pump
esp_mqtt_client_handle_t client;
char strbuf1[256];
	char strbuf2[256];
	int timer1 = 5;
	int timer2 = 30;
	int sensorOpen = 0;
static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
	//int t = 0;

	//char str[256];
	char buf[256] = "";
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            msg_id = esp_mqtt_client_subscribe(client, "/topic/dashboard/pump", 2);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            if(!strcmp(event->topic, "/topic/dashboard/pump")){
                sprintf(buf, "%.*s", event->data_len, event->data);
                msg_id = esp_mqtt_client_publish(client, "/topic/esppump", buf, 0, 0, 0);
                if(!strcmp(buf, "1")){
                    pumpOpen = timer2;
                } else if(!strcmp(buf, "0")){
                    pumpOpen = 0;
                }
            }
           /* printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
			//strcpy(buf, "");
			sprintf(buf, "%.*s", event->data_len, event->data);
			if(!(buf[0] == 'E' && buf[1] == 'C' && buf[2] == 'H' && buf[3] == 'O' && buf[4] == ':')){
				//str = "";
				sprintf(str, "ECHO: %.*s", event->data_len, event->data);
				msg_id = esp_mqtt_client_publish(client, "/topic/qos1", str, 0, 0, 0);

			}*/
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

static void mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = CONFIG_BROKER_URL,
    };
#if CONFIG_BROKER_URL_FROM_STDIN
    char line[128];

    if (strcmp(mqtt_cfg.uri, "FROM_STDIN") == 0) {
        int count = 0;
        printf("Please enter url of mqtt broker\n");
        while (count < 128) {
            int c = fgetc(stdin);
            if (c == '\n') {
                line[count] = '\0';
                break;
            } else if (c > 0 && c < 127) {
                line[count] = c;
                ++count;
            }
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
        mqtt_cfg.uri = line;
        printf("Broker url: %s\n", line);
    } else {
        ESP_LOGE(TAG, "Configuration mismatch: wrong broker url");
        abort();
    }
#endif /* CONFIG_BROKER_URL_FROM_STDIN */
	mqtt_cfg.username = "esp32";
	mqtt_cfg.password = "mcU32";
    client = esp_mqtt_client_init(&mqtt_cfg);

    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
}

void app_main(void)
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("MQTT_EXAMPLE", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());


    mqtt_app_start();


	int hatchstate = 0;///0 - closed, 1 - open
	while(1){
		vTaskDelay(1000 / portTICK_PERIOD_MS);
		///water level sensor
        if(gpio_get_level(GPIO_WATER)){
			sensorOpen++;
			if(sensorOpen < timer1){

				sprintf(strbuf1, "Water Level HIGH, timer %d\n", timer1 - sensorOpen);
			}
		}
        else{
			sensorOpen = 0;

			if(pumpOpen <= 0){
				sprintf(strbuf1, "Water Level: LOW\n");
				msg_id = esp_mqtt_client_publish(client, "/topic/esppump", "0", 0, 0, 0);
			}
		}

		///bilge

		if(timer1 < sensorOpen){
            gpio_set_level(GPIO_PUMP, 1);//activate the pump
			sprintf(strbuf1, "Water Level HIGH, Pump active\n");
			msg_id = esp_mqtt_client_publish(client, "/topic/esppump", "1", 0, 0, 0);
			pumpOpen = timer2;
		}else if(0 < pumpOpen){
		    gpio_set_level(GPIO_PUMP, 1);//activate the pump
			pumpOpen--;
			sprintf(strbuf1, "Water Level LOW, Pump active for %d seconds\n", pumpOpen);
			msg_id = esp_mqtt_client_publish(client, "/topic/esppump", "1", 0, 0, 0);
		}else{
            gpio_set_level(GPIO_PUMP, 0);
		}

		///water level + pump
		msg_id = esp_mqtt_client_publish(client, "/topic/espwater", strbuf1, 0, 0, 0);

		///external humidity + temp
		if((!gpio_get_level(GPIO_HUMIDITY) || gpio_get_level(GPIO_TEMP)) && hatchstate == 0){//temp over 30 and humidity low(not raining)
            hatchstate = 1;
			msg_id = esp_mqtt_client_publish(client, "/topic/esphatch", "Open", 0, 0, 1);
			gpio_set_level(GPIO_HATCH1, 1);//open the hatch
			gpio_set_level(GPIO_HATCH2, 0);//open the hatch
		}else{//temp under 25 or humidity high
		    if(hatchstate){
                hatchstate = 0;
                gpio_set_level(GPIO_HATCH2, 1);//close the hatch
                gpio_set_level(GPIO_HATCH1, 0);//close the hatch
                msg_id = esp_mqtt_client_publish(client, "/topic/esphatch", "Closed", 0, 0, 1);
		    }
		}

    }

}
