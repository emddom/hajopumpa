/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_adc_cal.h"

int temprature_sens_read();
int hall_sens_read();

void app_main(void)
{
    int ival = hall_sens_read();
    int open = 0;
    printf("Started\n");
    while(1){

        vTaskDelay(1000 / portTICK_PERIOD_MS);
        int val = hall_sens_read() - ival;
        printf("hall effect: %5d ", val);
        if(val <= - 100 || val >= 100){
            if(!open){
                open = 1;
                printf("\nOpened!\n");
            }else
                printf("\tTemp: %.2f degrees\n", (temprature_sens_read() - 32.0) * 5.0 / 9.0);
        }else{
            if(open){
                printf("\nClosed!\n");
                open = 0;
            }
        }
        printf("\n");
    }

}
