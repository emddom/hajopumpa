import paho.mqtt.client as mqtt
import time

def on_connect(client, userdata, flags, rc): 
   print("Connected - rc:", rc) 
def on_message(client, userdata, message): 
   if str(message.topic) != pubtop: 
       print(str(message.topic), ": ", str(message.payload.decode("utf-8"))) 
def on_subscribe(client, userdata, mid, granted_qos): 
   print("Subscribed:", str(mid), str(granted_qos)) 
def on_unsubscribe(client, userdata, mid): 
   print("Unsubscribed:", str(mid)) 
def on_disconnect(client, userdata, rc): 
   if rc != 0: 
       print("Unexpected disconnection.") 
       
broker_address = "192.168.1.115" 
port = 1883 

client = mqtt.Client() 
client.on_subscribe = on_subscribe 
client.on_unsubscribe = on_unsubscribe 
client.on_connect = on_connect 
client.on_message = on_message 
time.sleep(1) # Sleep for a beat to ensure things occur in order 
user = input('Username: ') 
pw = input('Password: ') 
pubtop = input('Publish topic: ')
subtop = input('Subscribe topic: ')
client.username_pw_set(user, pw)
client.connect(broker_address, port)

client.loop_start() 
client.subscribe(subtop)
# Chat loop 
while True: 
    chat = input() 
    if chat == 'QUIT': 
        break
    elif chat == 'SUBSCRIBE':
        new_subtop = input('Subscribe to topic: ')
        client.subscribe(new_subtop)
    elif chat == 'UNSUBSCRIBE':
        unsubtop = input('Unsubscribe from topic: ')
        client.unsubscribe(unsubtop)
    elif chat == 'PUBLISH':
        pubtop = input('Publish to new topic: ')
    else:
        client.publish(pubtop, chat) 
# Disconnect and stop the loop! 
client.disconnect() 
client.loop_stop() 